module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // sprite: {
        //     all: {
        //         src: 'imag/source/*.png',
        //         dest: 'spritesheet.png',
        //         destCss: 'sprites.less'
        //     }
        // },

        less: {
            production: {
                files: {
                    "css/style.css": "less/style.less"
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['last 10 versions', 'Firefox > 15', 'Opera >= 12']
            },
            build: {
                src: 'css/style.css',
                dest: 'css/style.css'
            }
        },
        concat_css: {
            all: {
              src: ['css/*.css'],
              dest: 'css/prod/style.css'
            }
        },
        cssmin: {
            target: {
                files: {
                    'css/prod/style.min.css': 'css/prod/style.css'
                }
            }
        },
        concat: {
            dist: {
                src: ['js/*.js', '!js/jquery.min.js'],
                dest: 'js/prod/main.js'
            }
        },
        uglify: {
            build: {
                src: 'js/prod/main.js',
                dest: 'js/prod/main.min.js'
            }
        },
        imagemin : {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'img/',
                    src: ['**/*.{png,jpg,gif,svg}', '!prod/**'],
                    dest: 'img/prod/'
                }]
            }
        },
        watch: {
            styles: {
                files: ['less/*.less'],
                tasks: ['less', 'autoprefixer', /*'sprite',*/ 'concat_css', 'cssmin'/*, 'imagemin'*/],
                options: {
                    nospawn: true
                }
            },
            scripts: {
                files: ['js/*.js'],
                tasks: ['concat', 'uglify'/*, 'imagemin'*/],
                options: {
                    nospawn: true
                }
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('images', ['imagemin']);
};