$(document).ready(init);

$(window).scroll(function() {

    attachMenu();
    parallax();
});

function init() {

    // Slider
    $(".main-slider").owlCarousel({
        items: 1,
        loop: true,
        dots: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 600,
        animateOut: "fadeOut"
    });

    // Parallax scroll
    $(".blue-bg-1, blue-bg-2, blue-bg-3").each(function() {

        var e = $(this);
        
        parallax = function() {
            if ($(window).width() >= 768) {
                var s = e.offset().top,
                    o = $(window).scrollTop(),
                    a = -(s - o) / 4;
                a = -a;
                var i = "center " + a + "px";
                e.css({
                    backgroundPosition: i
                })
            } else e.css({
                backgroundPosition: "center"
            })
        };
        
        parallax();
    });

    // Show/Hide fixed header
    attachMenu();
}

function attachMenu() {

    var el_header = $('header');
    var el_header_submenu = $('.header-submenu');

    $(window).scroll(function() {

        if ($(window).scrollTop() > 400) {

            $(el_header).addClass('fixed').slideDown();
            //$(el_header_submenu).addClass('fixed').slideDown();
        } else if ($(window).scrollTop() > 150) {
            $(el_header).slideUp(150, function() {
                //$(el_header_submenu).hide();
                if ($(el_header).hasClass('fixed')) {
                    $(el_header).removeClass('fixed');
                    //$('#header-submenu').removeClass('fixed');
                }
            });
        } else {
            $(el_header).removeClass('fixed').slideDown(150);
            //$(el_header_submenu).removeClass('fixed').slideDown(150);
        }
    });
}